package com.mobileiron.tlvinterpreter;


/**
 *
 * Upper case Expression which operates on the value to convert the value to uppercase string
 *
 */
public class UppercaseExpression extends Expression {
    private Expression value;

    public UppercaseExpression(Expression value) {
        this.value = value;
    }

    @Override
    public String interpret() {
        return this.value.interpret().toUpperCase();
    }

    @Override
    public String toString() {
        return "UPPRCS";
    }
}
