package com.mobileiron.tlvinterpreter;

public class ValueExpression extends Expression {

    private String value;

    public ValueExpression(String value){
        this.value = value;
    }

    public ValueExpression(int value){
        this.value = Integer.toString(value);
    }

    @Override
    public String interpret() {
        return this.value.toString();
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
