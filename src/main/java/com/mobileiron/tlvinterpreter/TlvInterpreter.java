package com.mobileiron.tlvinterpreter;

import java.util.Stack;

/**
 *
 * Process each TLV line with a given grammer
 * the Interpreter pattern to break sentences into expressions which is evaluated and as a whole form the result.
 */
public class TlvInterpreter {
    public void process(String inputTlvLine){
        // Interpreter the input string based on the grammer
        String[] tokenList = inputTlvLine.split("-");
        Stack<Expression> stack = new Stack<Expression>();

        // The input expression has prefix, where operator comes before the operands
        for (int i = tokenList.length - 1 ; i >= 0 ; i--) {
            if(isOperator(tokenList[i])){
                Expression length = stack.pop();
                Expression value = stack.pop();
                Expression operator = getOperator(tokenList[i],value);
                if(operator != null) {
                    String result1 = operator.interpret();
                    Expression resultExpression = new ValueExpression(result1);
                    stack.push(resultExpression);
                    stack.push(operator);
                }
            }
            else{
                Expression value = new ValueExpression(tokenList[i]);
                stack.push(value);
            }
        }
        String operator = stack.pop().toString();
        if(isOperator(operator))
            System.out.println(operator + "-" + stack.pop().interpret());
        else
            System.out.println("Type not valid");
    }

    private boolean isOperator(String operator){
        boolean isOperator = false;
        switch (operator){
            case "UPPRCS":
                isOperator = true;
                break;
            case "REPLCE":
                isOperator = true;
                break;
        }
        return isOperator;
    }

    private Expression getOperator(String operator, Expression value){
        Expression operatorExpression = null;
        switch (operator){
            case "UPPRCS":
                operatorExpression = new UppercaseExpression(value);;
                break;
            case "REPLCE":
                operatorExpression = new ReplaceExpression(value);;
                break;
        }
        return operatorExpression;
    }
}
