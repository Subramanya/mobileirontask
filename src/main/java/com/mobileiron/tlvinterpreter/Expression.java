package com.mobileiron.tlvinterpreter;

/*
* Representation for a grammar.
* This representation is used to interpret TLV.
*/
public abstract class Expression {
    public abstract String interpret();
}
