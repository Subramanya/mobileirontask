package com.mobileiron.tlvinterpreter;

/**
 *
 * Replace Expression which operates on the value and replace it with THIS-STRING
 *
 */
public class ReplaceExpression extends Expression {
    private Expression value;

    public ReplaceExpression(Expression value) {
        this.value = value;
    }

    @Override
    public String interpret() {
        return "THIS STRING";
    }

    @Override
    public String toString() {
        return "REPLCE";
    }
}
