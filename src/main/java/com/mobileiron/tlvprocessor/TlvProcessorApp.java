package com.mobileiron.tlvprocessor;

import config.TlvProcessorConfig;
import com.mobileiron.tlvinterpreter.TlvInterpreter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.io.BufferedReader;

 /* In this app we use the Interpreter pattern to break TLV line into expressions (
  * either Upper case expression, replace or value) that can be evaluated and as a whole form the result.
  * Input streams can be configured in TlvProcessorConfig.
  */
public class TlvProcessorApp {
    public static void main(String[] args){
        // Get input stream config from App context
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(TlvProcessorConfig.class);
        BufferedReader bufferedReader = applicationContext.getBean(BufferedReader.class);
        String inputTlvLine;

        try {
            TlvInterpreter tlvInterpreter = new TlvInterpreter();
            while ((inputTlvLine = bufferedReader.readLine()) != null) {
                // Process each input TLV line
                tlvInterpreter.process(inputTlvLine);
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
