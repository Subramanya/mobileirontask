package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

@Configuration
public class TlvProcessorConfig {

    @Bean
    public BufferedReader bufferedReader(){
        return new BufferedReader(inputStreamReader());
    }

    @Bean
    public InputStreamReader inputStreamReader(){
        return new InputStreamReader(consoleStream());
    }

    public InputStream consoleStream(){
        return System.in;
    }

    public InputStream networkStream() {
        try {
            ServerSocket serverSocket = new ServerSocket(10001);
            Socket clientSocket = serverSocket.accept();
            return clientSocket.getInputStream();
        }catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
    }

}
