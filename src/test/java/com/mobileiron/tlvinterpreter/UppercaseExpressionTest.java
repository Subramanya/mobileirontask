package com.mobileiron.tlvinterpreter;

import org.junit.Assert;
import org.junit.Test;

public class UppercaseExpressionTest {
    @Test
    public void testProcessToUppercase(){
        String data = "abc";
        UppercaseExpression uppercaseExpression = new UppercaseExpression(new ValueExpression(data));
        Assert.assertEquals(uppercaseExpression.interpret(), data.toUpperCase());
    }
}
