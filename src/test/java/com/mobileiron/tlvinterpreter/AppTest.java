package com.mobileiron.tlvinterpreter;

import com.mobileiron.tlvprocessor.TlvProcessorApp;
import config.TlvProcessorConfig;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= TlvProcessorConfig.class)
public class AppTest {
    @Rule
    public final TextFromStandardInputStream systemInMock
            = TextFromStandardInputStream.emptyStandardInputStream();
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void testApp(){
        // This if it reads input from STDIN.
        systemInMock.provideLines("UPPRCS-0004-data");
        TlvProcessorApp.main(new String[]{"data"});
        Assert.assertEquals("UPPRCS-DATA\n",systemOutRule.getLog() );
    }
}
