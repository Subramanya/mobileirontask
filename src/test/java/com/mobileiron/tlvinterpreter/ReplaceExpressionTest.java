package com.mobileiron.tlvinterpreter;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class ReplaceExpressionTest {
    @Test
    public void testReplace(){
        String data = "abc";
        ReplaceExpression uppercaseExpression = new ReplaceExpression(new ValueExpression(data));
        Assert.assertThat(uppercaseExpression.interpret(), CoreMatchers.is("THIS STRING"));
    }
}
