package com.mobileiron.tlvinterpreter;

import org.junit.Assert;
import org.junit.Test;

public class ValueExpressionTest {

    @Test
    public void testInterpretOnValueExpression(){
        String data = "abc";
        ValueExpression valueExpression = new ValueExpression(data);
        Assert.assertEquals(data,valueExpression.interpret());
    }
}
