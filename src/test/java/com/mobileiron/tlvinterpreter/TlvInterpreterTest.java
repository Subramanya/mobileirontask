package com.mobileiron.tlvinterpreter;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class TlvInterpreterTest {
    String upprcsData = "UPPRCS-0004-data";
    String replceData = "REPLCE-0004-data";
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    TlvInterpreter tlvInterpreter = new TlvInterpreter();

    @Test
    public void testProcessWithUpperCase(){
        tlvInterpreter.process(upprcsData);
        Assert.assertEquals("UPPRCS-DATA\n", systemOutRule.getLog());
    }

    @Test
    public void testProcessWithReplace(){
        tlvInterpreter.process(replceData);
        Assert.assertEquals("REPLCE-THIS STRING\n", systemOutRule.getLog());
    }

    @Test
    public void testProcessInvalidType(){
        String invalidTypeData = "INVALID-0004-data";
        tlvInterpreter.process(invalidTypeData);
        Assert.assertEquals("Type not valid\n", systemOutRule.getLog());
    }
}
