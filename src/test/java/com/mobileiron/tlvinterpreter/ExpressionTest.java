package com.mobileiron.tlvinterpreter;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class ExpressionTest {

    @Test
    public void testInterpretMethod(){
        Expression abstractExpression = Mockito.mock(Expression.class, Mockito.CALLS_REAL_METHODS);
        String data = "abcd";
        Mockito.doReturn(data.toUpperCase()).when(abstractExpression).interpret();
        Assert.assertThat(abstractExpression.interpret(), CoreMatchers.is(data.toUpperCase()));
    }
}
